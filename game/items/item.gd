extends Area2D

const ITEM_SFX_SCN = preload("res://items/ItemSFX.tscn")

class_name Item

enum Type {Ball, Bone, Duck, Sock}

export (Type) var type = Type.Ball
export(Texture) var spriteBall
export(Texture) var spriteBone
export(Texture) var spriteDuck
export(Texture) var spriteSock

func update_texture():
	if type == Type.Ball:
		$Sprite.set_texture(spriteBall)
	if type == Type.Bone:
		$Sprite.set_texture(spriteBone)
	if type == Type.Duck:
		$Sprite.set_texture(spriteDuck)
	if type == Type.Sock:
		$Sprite.set_texture(spriteSock)

func _ready():
	update_texture()

func _on_Area2D_body_entered(body):
	if not body.is_in_group("player"): return
	if body.type == self.type and body == get_node('/root/Main/World/TurnManager').cur_doggo:
		body.score += 1
		body.change_item_color(type)
		self.get_parent().add_child(ITEM_SFX_SCN.instance())
		self.queue_free()
