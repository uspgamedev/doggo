extends Button

func _on_dog4Button_mouse_entered():
	get_parent().get_node("selectorRoot").position = Vector2(510,0)
	get_parent().doggo = 3

func _on_dog4Button_button_down():
	if not get_parent().numberPhase:
		get_parent().selectDoggo()
