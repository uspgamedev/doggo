extends Button

func _on_dog1Button_mouse_entered():
	get_parent().get_node("selectorRoot").position = Vector2(0,0)
	get_parent().doggo = 0
	
func _on_dog1Button_button_down():
	if not get_parent().numberPhase:
		get_parent().selectDoggo()
