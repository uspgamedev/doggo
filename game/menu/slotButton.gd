extends Button

func _on_slotButton_button_down():
	if get_parent().numberPhase:
		get_parent().changePhase()
	else:
		get_parent().goBack()
