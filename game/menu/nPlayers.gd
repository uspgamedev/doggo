extends Sprite

onready var left = preload("res://assets/menu/left.png")
onready var leftGrey = preload("res://assets/menu/leftGrey.png")
onready var right = preload("res://assets/menu/right.png")
onready var rightGrey = preload("res://assets/menu/rightGrey.png")
onready var slot = preload("res://assets/menu/slot.png")
onready var slotGrey = preload("res://assets/menu/slotGrey.png")
onready var select = preload("res://assets/menu/selectDog.png")
onready var dog1 = preload("res://assets/menu/dog1.png")
onready var dog2 = preload("res://assets/menu/dog2.png")
onready var dog3 = preload("res://assets/menu/dog3.png")
onready var dog4 = preload("res://assets/menu/dog4.png")
onready var dogGrey = preload("res://assets/menu/dogGrey.png")
onready var blank = preload("res://assets/menu/blank_texture.png")
onready var global = get_node('/root/global')

onready var seletor = $selectorRoot

var m = 0 #jogador que está escolhendo
var numberPhase = true 
var doggo = 0 #cachorro que está sendo selecionado pelo cursor

func _input(event):
	if numberPhase:
		if event.is_action_pressed("ui_left"):
			if global.n > 2:
				global.n -= 1
				get_node("../SFXs").play_cursor()
		if event.is_action_pressed("ui_right"):
			if global.n < 4:
				global.n += 1
				get_node("../SFXs").play_cursor()
		if event.is_action_pressed("ui_accept"):
			changePhase()
					
			
func _physics_process(delta):
	if m == global.n: #INICIAR O JOGO 
		set_physics_process(false)
		yield(get_tree().create_timer(1), "timeout")
		get_tree().change_scene('res://play/Main.tscn')

	if numberPhase:
		$Label.text = str(global.n)
		if global.n == 2: $left.set_texture(leftGrey)
		else: $left.set_texture(left)
		if global.n == 4: $right.set_texture(rightGrey)
		else: $right.set_texture(right)
	else:
		get_node("selectorRoot/playerLabel/numberLabel").text = str(m+1)
		if Input.is_action_just_pressed("ui_accept"):
			selectDoggo()
		if Input.is_action_just_pressed("ui_right") and doggo < 3:
			seletor.position.x += 170
			doggo+=1
			get_node("../SFXs").play_cursor()
		if Input.is_action_just_pressed("ui_left") and doggo > 0:
			seletor.position.x -= 170
			doggo-=1
			get_node("../SFXs").play_cursor()
		if Input.is_action_just_pressed("ui_cancel"):
			goBack()

func changePhase():
	get_node("../SFXs").play_confirm()
	$right.set_texture(rightGrey)
	$left.set_texture(leftGrey)
	$slot.set_texture(slotGrey)
	$selectDog.set_texture(select)
	$dog1.set_texture(dog1)
	$dog2.set_texture(dog2)
	$dog3.set_texture(dog3)
	$dog4.set_texture(dog4)
	$selectorRoot.visible = true
	$likes.visible = true
	$ballIcon.visible = true
	$boneIcon.visible = true
	$duckIcon.visible = true
	$socksIcon.visible = true
	yield(get_tree().create_timer(0.5), "timeout")
	numberPhase = false
	
func goBack():
	if global.n < 4: $right.set_texture(right)
	if global.n > 2: $left.set_texture(left)
	$slot.set_texture(slot)
	$selectDog.set_texture(blank)
	$dog1.set_texture(blank)
	$dog2.set_texture(blank)
	$dog3.set_texture(blank)
	$dog4.set_texture(blank)
	$selectorRoot.visible = false
	$likes.visible = false
	$ballIcon.visible = false
	$boneIcon.visible = false
	$duckIcon.visible = false
	$socksIcon.visible = false
	m = 0
	global.playerChoices = [null,null,null,null]
	numberPhase = true
	
func selectDoggo():
	if not doggo in global.playerChoices:
		global.playerChoices[m] = doggo
		get_node("../SFXs").play_bark(doggo)
		m+=1
		greyOutDoggoSprite(doggo)
	else:
		#Tocar um sfx de NOPE
		pass

func greyOutDoggoSprite(doggo):
	if doggo == 0:
		$dog1.set_texture(dogGrey)
	if doggo == 1:
		$dog2.set_texture(dogGrey)
	if doggo == 2:
		$dog3.set_texture(dogGrey)
	if doggo == 3:
		$dog4.set_texture(dogGrey)
