extends Button

func _on_dog3Button_mouse_entered():
	get_parent().get_node("selectorRoot").position = Vector2(340,0)
	get_parent().doggo = 2

func _on_dog3Button_button_down():
	if not get_parent().numberPhase:
		get_parent().selectDoggo()
