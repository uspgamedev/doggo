extends Node2D

var done = false

func _ready():
	yield(get_tree().create_timer(0.5), "timeout")
	$terminal_cursor.visible = true	
	for l in range(7):
		yield(get_tree().create_timer(0.1), "timeout")
		$Label.visible_characters += 1
		$terminal_cursor.position.x += 50
	yield(get_tree().create_timer(0.5), "timeout")
	$terminal_cursor.visible = false
	$blinking_cursor.visible = true
	yield(get_tree().create_timer(1), "timeout")
	$enter.visible = true
	done = true	
	
func _process(delta):
	if Input.is_action_just_released("ui_accept") and done:
		get_tree().change_scene('res://menu/menu.tscn')
		
	


