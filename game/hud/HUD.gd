extends CanvasLayer

const TWEEN_DUR = .5
const TESTING = false
const TEST_CLOCK_TIMER = 3

onready var clock : TextureProgress = $Clock
onready var fade : ColorRect = $Fade
onready var player_turn : VBoxContainer = $Fade/PlayerTurnContainer
onready var player_turn_tween : Tween = $Fade/PlayerTurnContainer/Tween
onready var tween : Tween = $Tween

signal action_button_pressed
signal fade_from_black_ended
signal fade_to_black_ended
signal player_turn_intro_ended

func _ready():
	print(global.playerChoices)
	get_node("PlayerContainer/Player1/Portrait").texture = load(str("res://assets/menu/dog", str(global.playerChoices[0]+1) , ".png"))
	get_node("PlayerContainer/Player1/Items/Item1").texture = get_item_texture(global.playerChoices[0])
	get_node("PlayerContainer/Player1/Items/Item2").texture = get_item_texture(global.playerChoices[0])
	get_node("PlayerContainer/Player1/Items/Item3").texture = get_item_texture(global.playerChoices[0])
	get_node("PlayerContainer/Player1").item_icon = get_item_texture(global.playerChoices[0])
	get_node("PlayerContainer/Player2/Portrait").texture = load(str("res://assets/menu/dog", str(global.playerChoices[1]+1) , ".png"))
	get_node("PlayerContainer/Player2/Items/Item1").texture = get_item_texture(global.playerChoices[1])
	get_node("PlayerContainer/Player2/Items/Item2").texture = get_item_texture(global.playerChoices[1])
	get_node("PlayerContainer/Player2/Items/Item3").texture = get_item_texture(global.playerChoices[1])
	get_node("PlayerContainer/Player2").item_icon = get_item_texture(global.playerChoices[1])
	if global.playerChoices[2] != null:
		get_node("PlayerContainer/Player3/Portrait").texture = load(str("res://assets/menu/dog", str(global.playerChoices[2]+1) , ".png"))
		get_node("PlayerContainer/Player3/Items/Item1").texture = get_item_texture(global.playerChoices[2])
		get_node("PlayerContainer/Player3/Items/Item2").texture = get_item_texture(global.playerChoices[2])
		get_node("PlayerContainer/Player3/Items/Item3").texture = get_item_texture(global.playerChoices[2])
		get_node("PlayerContainer/Player3").item_icon = get_item_texture(global.playerChoices[2])
	else:
		get_node("PlayerContainer/Player3").visible = false
	if global.playerChoices[3] != null:
		get_node("PlayerContainer/Player4/Portrait").texture = load(str("res://assets/menu/dog", str(global.playerChoices[3]+1) , ".png"))
		get_node("PlayerContainer/Player4/Items/Item1").texture = get_item_texture(global.playerChoices[3])
		get_node("PlayerContainer/Player4/Items/Item2").texture = get_item_texture(global.playerChoices[3])
		get_node("PlayerContainer/Player4/Items/Item3").texture = get_item_texture(global.playerChoices[3])
		get_node("PlayerContainer/Player4").item_icon = get_item_texture(global.playerChoices[3])
	else:
		get_node("PlayerContainer/Player4").visible = false
	
func get_item_texture(doggo_num):
	match doggo_num:
		0:
			return load("res://hud/player-info/ball.png")
		1:
			return load("res://hud/player-info/bone.png")
		2:
			return load("res://hud/player-info/duck.png")
		3:
			return load("res://hud/player-info/socks.png")
		
	
func _unhandled_input(event):
	if event.is_action_pressed("ui_accept"):
		emit_signal("action_button_pressed")
	
	if !TESTING:
		return
	elif Input.is_key_pressed(KEY_1):
		player_turn_intro(1)
	elif Input.is_key_pressed(KEY_2):
		player_turn_intro(2)
	elif Input.is_key_pressed(KEY_3):
		player_turn_intro(3)
	elif Input.is_key_pressed(KEY_4):
		player_turn_intro(4)


func update_clock(value : float):
	clock.value = value * clock.max_value


func fade_from_black():
	tween.interpolate_property(fade, "color", null, Color(0, 0, 0, 0),
			TWEEN_DUR, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("fade_from_black_ended")


func fade_to_black():
	tween.interpolate_property(fade, "color", null, Color(0, 0, 0, 1),
			TWEEN_DUR, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("fade_to_black_ended")


func player_turn_intro(player : int):
	
	
	
	var player_info : PlayerInfo = get_node(str("PlayerContainer/Player",
			player)) as PlayerInfo
	$Fade/PlayerTurnContainer/HBox/ItemIcon.texture = player_info.item_icon
	$Fade/PlayerTurnContainer/TurnLabel.text = "Player %s's turn!" % player
	
	for p in $PlayerContainer.get_children():
		p.selected(p == player_info)
	
	tween.interpolate_property(fade, "color", null, Color(0, 0, 0, .5),
			TWEEN_DUR, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	
	player_turn.modulate = Color(1, 1, 1, 0)
	player_turn.visible = true
	player_turn_tween.interpolate_property(player_turn, "modulate", null,
			Color(1, 1, 1), TWEEN_DUR, Tween.TRANS_LINEAR, Tween.EASE_IN)
	player_turn_tween.start()
	
	yield(player_turn_tween, "tween_completed")
	yield(self, "action_button_pressed")
	
	tween.interpolate_property(fade, "color", null, Color(0, 0, 0, 0),
			TWEEN_DUR, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	
	player_turn_tween.interpolate_property(player_turn, "modulate", null,
			Color(1, 1, 1, 0), TWEEN_DUR, Tween.TRANS_LINEAR, Tween.EASE_IN)
	player_turn_tween.start()
	
	yield(player_turn_tween, "tween_completed")
	
	player_turn.visible = false
	emit_signal("player_turn_intro_ended")
