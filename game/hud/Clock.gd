extends TextureProgress

export var timer_path : NodePath

func _process(delta):
	if has_node(self.timer_path):
		var timer : Timer = get_node(self.timer_path)
		var progress = timer.time_left / timer.wait_time
		self.radial_fill_degrees = progress * 360
