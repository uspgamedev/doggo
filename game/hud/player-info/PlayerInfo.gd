tool
extends VBoxContainer

class_name PlayerInfo

enum FacingDirection {LEFT, RIGHT}

export(FacingDirection) var facing_direction = FacingDirection.LEFT setget set_direction
export(Texture) var item_icon : Texture

var items_obtained = 0

onready var items : HBoxContainer = $Items

func update_item_alpha(score):
	get_node(str('Items/Item', str(score))).modulate = Color(1, 1, 1, 1)

func _ready():
	for item in items.get_children():
		(item as TextureRect).texture = item_icon
		item.visible = true

func get_item():
	assert(items_obtained < items.get_child_count())
	items.get_child(items_obtained).visible = true
	items_obtained += 1

func set_direction(dir):
	facing_direction = dir
	$Portrait.rect_scale.x = 1 if dir == FacingDirection.LEFT else -1

func selected(is_selected):
	$Portrait/Highlight.visible = is_selected
