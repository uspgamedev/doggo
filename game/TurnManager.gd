extends Node

var doggos = [null, null, null, null]

signal start_turn
signal turn_ended(dog_number)

var cur_doggo
onready var world = get_parent()
onready var timer = $TurnTimer
onready var global = get_node('/root/global')

func _on_TurnTimer_timeout():
	$TurnEndSFX.play()
	var prev_room = cur_doggo.get_parent().get_parent()
	cur_doggo.lockControls()
	if world.on_transition:
		yield(world, 'end_transition')
	var idx = doggos.find(cur_doggo)
	idx = wrapi(idx +1, 0, global.n)
	cur_doggo = doggos[idx]
	yield(get_tree(), 'physics_frame')
	var cur_room = cur_doggo.get_parent().get_parent()
	print('prev_room.name ', prev_room.name)
	print('cur_room.name ', cur_room.name)
	world.transition(prev_room, cur_room, randi()%4)
	if world.on_transition:
		yield(world, 'end_transition')
	emit_signal("turn_ended", idx + 1)

func begin_turn():
	cur_doggo.unlockControls()
	timer.start()
