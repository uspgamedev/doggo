extends KinematicBody2D

const ACC_FACTOR = .3
const SPEED = 250
const SPECIAL_IDLE_CHANCE = .14

onready var animation : AnimationPlayer = $AnimationPlayer
onready var head_animation : AnimationPlayer = $Body/Head/AnimationPlayer
onready var tail_animation : AnimationPlayer = $Body/Tail/AnimationPlayer
onready var body_sprite : Sprite = $Body
onready var shadow_sprite : Sprite = $Shadow
onready var last_spot = self.position

var movement : Vector2   = Vector2()
var score    : int       = 0
var type = 0

var particles_scn = preload('res://doggo/particles.tscn')

func _ready():
	lockControls()
	add_to_group("player")
	head_animation.connect("animation_finished", self, "_on_head_animation_finished")
	body_sprite.material.set_shader_param("hue", get_hue())
	print(body_sprite.material.get_shader_param("hue"))

func get_hue():
	match type:
		0:
			return 0.069
		1:
			return 0.777
		2:
			return 0.325
		3:
			return 0.005

func change_item_color(type):
	var hud = get_node("/root/Main/HUD")
	var idx = global.playerChoices.find(type)
	hud.get_node(str('PlayerContainer/Player', str(idx+1))).update_item_alpha(score)

func _physics_process(delta):
	movement = movement.linear_interpolate(get_direction() * SPEED, ACC_FACTOR)
	movement = move_and_slide(movement)
	
	var particle_dir = -1
	
	if movement.x > 0:
		shadow_sprite.scale.x = - abs(shadow_sprite.scale.x)
		body_sprite.scale.x = - abs(body_sprite.scale.x)
	elif movement.x < 0:
		particle_dir = 1
		shadow_sprite.scale.x = abs(shadow_sprite.scale.x)
		body_sprite.scale.x = abs(body_sprite.scale.x)

	if self.position.distance_to(last_spot) > 10:
		last_spot = self.position
		var particles = particles_scn.instance()
		get_parent().add_child(particles)
		particles.position = last_spot + Vector2(0, 15)
		particles.emitting = true
		particles.one_shot = true
		particles.scale = Vector2(particle_dir, 1)

func get_direction() -> Vector2:
	var direction : Vector2 = Vector2()
	
	if Input.is_action_pressed("ui_left"):
		direction += Vector2(-1, 0)
	if Input.is_action_pressed("ui_right"):
		direction += Vector2(1, 0)
	if Input.is_action_pressed("ui_up"):
		direction += Vector2(0, -1)
	if Input.is_action_pressed("ui_down"):
		direction += Vector2(0, 1)
	
	if direction.length() == 0 and not animation.current_animation.begins_with("idle"):
		for anim in [animation, head_animation, tail_animation]:
			(anim as AnimationPlayer).play("idle")
	elif direction.length() != 0 and not animation.current_animation.begins_with("walk"):
		for anim in [animation, head_animation, tail_animation]:
			(anim as AnimationPlayer).play("walk")
	
	return direction.normalized()


func lockControls():
	set_physics_process(false)
	for anim in [animation, head_animation, tail_animation]:
		(anim as AnimationPlayer).play("idle")

func unlockControls():
	set_physics_process(true)


func _on_head_animation_finished(anim_name : String):
	if anim_name.begins_with("idle"):
		anim_name = "idle" if randf() > SPECIAL_IDLE_CHANCE else "idle_special"
		head_animation.play(anim_name)
		
