extends Node2D

var RoomPacks  = []
var viewport_size : Vector2
var AllRooms   = []
var on_transition = false
var Generator = null
onready var global = get_node('/root/global')
onready var turn_manager = $TurnManager
var ItemPack = null
signal end_transition
var won_already = false
const anim_duration = 1

func _ready():
	randomize()
	ItemPack  = preload("res://items/item.tscn")
	Generator = preload("res://scripts/rooms/map_generator.gd").new()
	# place rooms here  vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	RoomPacks.push_back(preload("res://rooms/basement/Basement.tscn"))
	RoomPacks.push_back(preload("res://rooms/bathroomA/BathroomA.tscn"))
	RoomPacks.push_back(preload("res://rooms/bathroomB/BathroomB.tscn"))
	RoomPacks.push_back(preload("res://rooms/cellar/Cellar.tscn"))
	RoomPacks.push_back(preload("res://rooms/couple-bedroom/CoupleBedroom.tscn"))
	RoomPacks.push_back(preload("res://rooms/dining-room/DiningRoom.tscn"))
	RoomPacks.push_back(preload("res://rooms/game-room/GameRoom.tscn"))
	RoomPacks.push_back(preload("res://rooms/hallA/Hall.tscn"))
	RoomPacks.push_back(preload("res://rooms/hallB/HallB.tscn"))
	RoomPacks.push_back(preload("res://rooms/horizontal-corridor/HorizontalCorridor.tscn"))
	RoomPacks.push_back(preload("res://rooms/kitchen/Kitchen.tscn"))
	RoomPacks.push_back(preload("res://rooms/library/Library.tscn"))
	RoomPacks.push_back(preload("res://rooms/living-room/LivingRoom.tscn"))
	RoomPacks.push_back(preload("res://rooms/office/Office.tscn"))
	RoomPacks.push_back(preload("res://rooms/single-bedroom/SingleBedroom.tscn"))
	RoomPacks.push_back(preload("res://rooms/vertical-corridor/VerticalCorridor.tscn"))
	
	for i in global.n:
		turn_manager.doggos[i] = preload("res://doggo/Dog.tscn").instance()
	turn_manager.cur_doggo = turn_manager.doggos[0]
	
	generate_map()

func _process(delta):
	# window size may change (?)
	viewport_size = get_viewport().size
	
	for i in global.n:
		if turn_manager.doggos[i].score >= 3:
			global.winner = i
			global.type_winner = turn_manager.cur_doggo.type
			if not won_already:
				turn_manager.timer.paused = true
				$wait_before_win_screen.start()
				won_already = true
			break
	

func transition(from, to, orientation):
	if (on_transition): return false
	if (from == to): 
		print("    Abort transition animation due to origin-destination equality")
		return true# no slide required
	
	to.visible  = true
	
	var delta_pos : Vector2
	
	if (orientation == Teleporter.Orientation.North):
		delta_pos = Vector2(0, -1)
	elif (orientation == Teleporter.Orientation.South):
		delta_pos = Vector2(0,  1)
	elif (orientation == Teleporter.Orientation.West):
		delta_pos = Vector2(-1, 0)
	else: # right
		delta_pos = Vector2(1,  0)
	delta_pos = delta_pos * 500 + delta_pos * viewport_size
	
	# this places the goal in a position which will land on the spot we want
	to.position = from.position + delta_pos
	
	var from_tween = from.get_node("Transition_Tween")
	var   to_tween =   to.get_node("Transition_Tween")
	
	from_tween.interpolate_property(from, "position", from.position, from.position - delta_pos, anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	to_tween.interpolate_property(to,     "position", to.position  , to.position   - delta_pos, anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	
	from_tween.start()
	to_tween.start()
	
	get_node('../whirl/AnimationPlayer').play('fucc')
	
	on_transition = true
	
	turn_manager.cur_doggo.lockControls()
	turn_manager.timer.paused = true
	
	# wait for animation to finish
	yield(to_tween, "tween_completed")
	
	if turn_manager.timer.time_left != 0:
		turn_manager.cur_doggo.unlockControls()
	turn_manager.timer.paused = false
	
	emit_signal('end_transition')
	on_transition = false
	
	# now hide old cell 
	from.visible = false
	from.position = Vector2(-10000, -10000) - Vector2(randi()%10000, randi()%10000)
	return true

func pick_random(list):
	if list.size() == 0:
		return null
	if list.size() == 1:
		return list[0]
	return list[randi() % list.size()]

func generate_map():
	#select rooms to spawn 	
	var num_rooms = 12
	var num_items = 3
	var actual_rooms = []
	
	var used = {}
	while (actual_rooms.size() < num_rooms):
		var pack = pick_random(RoomPacks)
		while used.has(pack):
			pack = pick_random(RoomPacks)
		var new_room = pack.instance()
		new_room.set_world(self)
		actual_rooms.push_back(new_room)
		used[pack] = true
		
	Generator.generate_map(actual_rooms, self)
	
	for child in get_children():
		if child is Room:
			AllRooms.append(child)
	
	print(AllRooms)
	
	var spawn = pick_random(AllRooms)
	spawn.position = OS.window_size/2
	var ysort = spawn.get_node('YSort')
	for i in global.n:
		var spawner = pick_random(spawn.get_spawners())
		turn_manager.doggos[i].position = spawner.position
		turn_manager.doggos[i].type = global.playerChoices[i]
		spawner.queue_free()
		ysort.add_child(turn_manager.doggos[i])
		spawn.visible = true
		
	for i in global.n:
		for j in num_items:
			var item = ItemPack.instance()
			item.type = global.playerChoices[i]
			var room = pick_random(AllRooms)
			room.add_child(item)
			var spawner = pick_random(room.get_spawners())
			item.position = spawner.position
			spawner.queue_free()
			

func _on_wait_before_win_screen_timeout():
	get_tree().change_scene("res://Win.tscn")
