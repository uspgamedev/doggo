extends Node2D
class_name Room
# if present, will set the teleporter to this location
# strings seem to be needed because of NodePath 
# limitations/my-ignorance.

func get_spawners():
	var val = []
	for child in get_children():
		if child is Spawner:
			val.append(child)
	return val

func free_doors():
	var val = []
	for door in get_doors():
		if door.destination == null:
			val.push_back(door)
	return val

func get_doors():
	var val = []
	for child in get_children():
		if child is Teleporter:
			val.push_back(child)
	return val
	
func num_doors():
	var num = 0
	for child in get_children():
		if child is Teleporter:
			num += 1
	return num

func set_world(world):
	for child in get_children():
		if child is Teleporter:
			child.world = world

func has_door_at(direction):
	return get_door_at(direction) != null

func get_door_at(direction):
	for child in get_children():
		if child is Teleporter and child.orientation == direction:
			return child
	return null