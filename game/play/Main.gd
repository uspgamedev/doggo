extends Control

func _ready():
	$World/TurnManager.connect("turn_ended", $HUD, "player_turn_intro")
	$HUD.connect("player_turn_intro_ended", $World/TurnManager, "begin_turn")
	
	$HUD.player_turn_intro(1)
