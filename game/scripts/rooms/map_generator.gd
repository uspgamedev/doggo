extends Node

const THRESHOLD           = 3
const EXPAND_CREATE_RATIO = 7

func pick_random(list):
	if list.size() == 0:
		return null
	if list.size() == 1:
		return list[0]
	return list[randi() % list.size()]

func get_free_doors(system):
	var free_doors = []
	for room in system:
		free_doors += room.free_doors()
	return free_doors

func __glue_sublists(list_a, list_b):
	var removed_indices = []
	for door in list_a:
		var _match = pick_random(list_b)
		if  _match != null:
			door.destination = _match
			_match.destination = door
			removed_indices.append(list_a.find(door))
			list_b.erase(_match)
	removed_indices.invert()
	for i in removed_indices:
		list_a.remove(i)

func glue_possible_doors(door_list):
	var w_doors = [] 
	var e_doors = []
	var n_doors = [] 
	var s_doors = []
	
	# sort the orientations
	for door in door_list:
		if door.orientation == Teleporter.Orientation.West:
			w_doors.push_back(door)
		if door.orientation == Teleporter.Orientation.East:
			e_doors.push_back(door)
		if door.orientation == Teleporter.Orientation.North:
			n_doors.push_back(door)
		if door.orientation == Teleporter.Orientation.South:
			s_doors.push_back(door)
	
	# it should suffice to glue one to the other,
	# the symetrical case is not needed
	__glue_sublists(w_doors, e_doors)
	__glue_sublists(n_doors, s_doors)
	
	# the remaining lists should replace the door_list, no?
	door_list = w_doors + e_doors + n_doors + s_doors

func generate_map(room_pool, world):
	print("generating map")
	
	var consecutive_failures = 0
	
	var initial_room = pick_random(room_pool)
	room_pool.erase(initial_room)
	var system = [ initial_room ]
	
	while (room_pool.size() > 0):
#		print(consecutive_failures)
		# Glue the system's free doors among themselves
		var sys_free_doors = get_free_doors(system)
		var old_size = sys_free_doors.size()+1 # +1 ensures it will enter the loop
		while old_size != sys_free_doors.size():
			sys_free_doors = get_free_doors(system)
			old_size = sys_free_doors.size()
			glue_possible_doors(sys_free_doors)
		
		var new_room = pick_random(room_pool)
		if new_room == null: break # just exit
		for room in system:
			# if the new room fits between two rooms, glue it. 
			for new_door in new_room.free_doors():
				var old_door = room.get_door_at(new_door.opposite_dir())
				if old_door != null:
					var _new_door = new_room.get_door_at(old_door.orientation)
					if _new_door != null and _new_door.destination == null:
						_new_door.destination  = old_door.destination
						if (old_door.destination != null):
							_new_door.destination.destination = _new_door
						
						old_door.destination  = new_door
						new_door.destination  = old_door
						
						if system.find(new_room) < 0:
							consecutive_failures = 0
							room_pool.erase(new_room)
							system.append(new_room)
		
		# if we couldn't stick it in between rooms, then let's 
		# try putting it somewhere and gluing stuff to it. 
		if system.find(new_room) < 0:
			for room in system: 
				for door in new_room.free_doors():
					var old_door = room.get_door_at(door.opposite_dir())
					if old_door != null and old_door.destination == null:
						old_door.destination = door
						door.destination = old_door
						
						if system.find(new_room) < 0:
							consecutive_failures = 0
							system.append(new_room)
							room_pool.erase(new_room)
		
		# avoids infinite loops
		if system.find(new_room) < 0:
			consecutive_failures += 1
		if consecutive_failures > THRESHOLD:
			break
	
	# tidy things
	glue_possible_doors(get_free_doors(system))
	
	for room in system:
		for door in room.free_doors():
			door.queue_free()
	
	for room in system:
		world.add_child(room)
		room.position = Vector2(-10000, -10000)
		room.visible = false