extends Node2D
class_name Teleporter

enum Orientation {West, East, North, South}

export (Orientation) var orientation = Orientation.West
var destination = null
var world = null

func opposite_dir():
	if orientation == Orientation.West:
		return Orientation.East
	if orientation == Orientation.East:
		return Orientation.West
	if orientation == Orientation.North:
		return Orientation.South
	if orientation == Orientation.South:
		return Orientation.North

func get_room():
	return get_parent()

func _deferred_swap_nodes(player, cell):
	player.get_parent().remove_child(player)
	cell.get_node('YSort').add_child(player)

func do_teleport(player):
	if not player.is_in_group("player"): return
	if not player == world.get_node("TurnManager").cur_doggo: 
		print("bolovo")
		return
	
	if (not $Cooldown.is_stopped()): 
		print("  Cooldown")
		return
	if (not destination != null):    
		print("  null destination")
		return
	if (not world != null):
		print("  null world")
		return
	
	# teleports the player to the apropriate room
	var current_player_cell = self.get_parent()
	var next_player_cell    = destination.get_parent()
	
	# do animation, should yield
	print("in transition to : ", current_player_cell.name, " , ", next_player_cell.name)
	print("   @", self.name,     " -> ", destination.name)
	print("   @", self.global_position, " -> ", destination.global_position)
	var success = world.transition(current_player_cell, next_player_cell, orientation)
	
	if not success: return
	
	# now activate cooldowns, so the player won't blink to and fro
	$Cooldown.start()
	destination.get_node("Cooldown").start()
	
	# seems necessary
	call_deferred("_deferred_swap_nodes", player, next_player_cell)
	
	# this is so the player doesn't spawn over the door
	var door_delta : Vector2
	if (orientation == Orientation.North):
		door_delta = Vector2( 0,-1)
	elif (orientation == Orientation.South):
		door_delta = Vector2( 0,+1)
	elif (orientation == Orientation.West):
		door_delta = Vector2(-1, 0)
	elif (orientation == Orientation.East):
		door_delta = Vector2(+1, 0)
	else:
		door_delta = Vector2()
	door_delta *= 64 # door size
	
	player.position = destination.position + door_delta
	$TransitionSFX.play()

func _on_teleport_body_entered(body):
	do_teleport(body)