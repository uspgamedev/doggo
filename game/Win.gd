extends Node2D

onready var global = get_node('/root/global')

func _ready():
	$Dog.lockControls()
	for child in get_children():
		if child is Item:
			child.type = global.winner
			child.update_texture()
			child.rotation = randf()*180
	get_node('Dog').type = global.type_winner
	get_node('Dog').body_sprite.material.set_shader_param("hue", get_node('Dog').get_hue())
	get_node('Item/Sprite').texture = get_item_texture(global.type_winner)
	get_node('Item2/Sprite').texture = get_item_texture(global.type_winner)
	get_node('Item3/Sprite').texture = get_item_texture(global.type_winner)

func get_item_texture(doggo_num):
	match doggo_num:
		0:
			return load("res://items/tennis-ball.png")
		1:
			return load("res://items/bone.png")
		2:
			return load("res://items/rubber-duck.png")
		3:
			return load("res://items/socks.png")

func _on_fanfare_finished():
	$Timer.start()

func _on_should_change_scene():
	global.playerChoices = [null,null,null,null]
	get_tree().change_scene("res://menu/menu.tscn")